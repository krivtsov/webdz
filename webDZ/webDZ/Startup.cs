﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webDZ.Startup))]
namespace webDZ
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
