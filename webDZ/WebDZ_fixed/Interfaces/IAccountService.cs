﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDZ_fixed.Models;

namespace WebDZ_fixed.Interfaces
{
    interface IAccountService
    {
        int CreateAccount(RegisterAccount model);

        void UpdateAccount(RegisterAccount model);

        RegisterAccount GetAccountById(int id);
    }
}
