﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SQLDatabase.Models;
using WebDZ_fixed.Models;

namespace WebDZ_fixed.Mappings
{
    public class RegistrationMappings : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<RegisterAccount, User>();
        } 
    }
}