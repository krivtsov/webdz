﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace WebDZ_fixed.Utilities
{
    public class JsonNetResult : JsonResult
    {
        public JsonSerializerSettings Settings { get; set; }

        public object ResponseBody { get; set; }

        private Formatting Formatting
        {
            get { return Debugger.IsAttached ? Formatting.Indented : Formatting.None; }
        }
        public JsonNetResult()
        {
        }

        public JsonNetResult(object responseBody)
        {
            ResponseBody = responseBody;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            // set content type 
            if (!string.IsNullOrEmpty(this.ContentType))
            {
                response.ContentType = this.ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }

            // set content encoding 
            if (this.ContentEncoding != null)
            {
                response.ContentEncoding = this.ContentEncoding;
            }

            if (this.ResponseBody != null)
            {
                response.Write(JsonConvert.SerializeObject(this.ResponseBody, this.Formatting, this.Settings));
            }
        }
    }
}