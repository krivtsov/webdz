﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using AutoMapper;
using WebDZ_fixed.Models;
using WebDZ_fixed.Utilities;
using SQLDatabase.Context;
using SQLDatabase.Models;
using WebDZ_fixed;
using WebDZ_fixed.Mappings;
using WebDZ_fixed.Services;


namespace WebDZ_fixed.Controllers
{

    [AllowAnonymous]
    public class AccountController : Controller
    {

        TestApplicationContext context = new TestApplicationContext();
        AccountService accountService = new AccountService();
        // GET: Account
        [HttpGet]
        public ActionResult Login(string message)
        {
            @ViewBag.message = message;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginAccount model)
        {
            if (ModelState.IsValid)
            {
                var account = context.Users.FirstOrDefault(x => x.Email == model.Email && x.Password == model.Password);
                if (account != null)
                {
                    FormsAuthentication.SetAuthCookie(model.Email, true);
                    return RedirectToAction("AdminOrHome", "Account");
                }
                else
                {
                    return RedirectToAction("Login", "Account", new {message = "Login or password is failed"});

                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterAccount user)
        {
            try
            {
                accountService.CreateAccount(user);
                return View();
            }
            catch (Exception ex)
            {
                return Content("404");
            }
        }



        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("index", "Home");
        }

        public ActionResult AdminOrHome()
        {
            return View();
        }
    }
}