﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SQLDatabase.Context;
using SQLDatabase.Models;

namespace WebDZ_fixed.Controllers
{
    public class HomeController : Controller
    {
        TestApplicationContext context = new TestApplicationContext();
        public HomeController()
        {
            
        }
        // GET: Home
        public ActionResult Index()
        {
           
                return View();
            
        }

        public ActionResult Home()
        {
            var score = AutocompleteSearchScore();
            return View();
        }

        public ActionResult AutocompleteSearch(string term)
        {
            var clients = context.Clients.Where(x => x.Name.Contains(term)).Select(a => new {value = a.Name}).Distinct();
            return Json(clients, JsonRequestBehavior.AllowGet);
        }




        public Score AutocompleteSearchScore(string term = "")
        {
            var clients = context.Clients.Single(x => x.Name == term);
            if (clients != null)
            {
                var score = context.Scores.FirstOrDefault(x => x.Id == clients.ScoreId);
                if (score != null)
                {
                    return score;
                }
                return null;
            }
            return null;
        }
    }
}