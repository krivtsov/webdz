﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SQLDatabase.Context;
using SQLDatabase.Models;
using WebDZ_fixed.Models;
using WebDZ_fixed.Services;

namespace WebDZ_fixed.Controllers
{
   
    [Authorize]
    public class AdminController : Controller
    {
        TestApplicationContext context = new TestApplicationContext();
        AdminService adminService = new AdminService();
        // GET: Admin
        public ActionResult Index()
        {

            ViewBag.Clients = context.Clients.ToList();
            ViewBag.Scores = context.Scores.ToList();
            return View();
        }


        [HttpGet]
        public ActionResult CreateClient()
        {
            ViewBag.Score = context.Scores.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult CreateClient(ClientViewModel model)
        {
            try
            {
                adminService.CreateClient(model);
                return RedirectToAction("Index", "Admin");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult CreateScore()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CreateScore(ScoreViewModel model)
        {

            try
            {
                adminService.CreateScore(model);
                return RedirectToAction("Index", "Admin");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            
        }
        [HttpGet]
        public ActionResult UpdateClient(int id)
        {
            return View("CreateScore");
        }

        [HttpPost]
        public ActionResult UpdateClient(ClientViewModel model)
        {
            try
            {
               // adminService.UpdateClient(model);
                return View("Index");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        [HttpPost]
        public void AddSum(int id, int sum)
        {
            try
            {
                var client = context.Clients.FirstOrDefault(x => x.Id == id);
                if(client != null)
                {
                    ScoreHistory model = new ScoreHistory { ClientId = client.Id, ScoreId = client.ScoreId, DescriptionScore = "wnfoiew", Summa = sum}
                context.Clients.Add(client);
                    context.SaveChanges();
                }

            }
        }
    }
}