﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebDZ_fixed.Models
{
    public class ClientViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int ScoreId { get; set; }
    }
}