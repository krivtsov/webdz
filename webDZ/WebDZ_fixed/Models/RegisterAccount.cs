﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebDZ_fixed.Models
{
    public class RegisterAccount
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int? Age { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}