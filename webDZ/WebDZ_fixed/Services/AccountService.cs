﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using SQLDatabase.Context;
using SQLDatabase.Models;
using WebDZ_fixed.Interfaces;
using WebDZ_fixed.Models;

namespace WebDZ_fixed.Services
{
    public class AccountService : IAccountService
    {

        TestApplicationContext context = new TestApplicationContext();
        public int CreateAccount(RegisterAccount model)
        {
            if (context.Users.FirstOrDefault(x => x.Email == model.Email) == null)
            {
                Mapper.CreateMap<RegisterAccount, User>();
                var account = Mapper.Map<RegisterAccount, User>(model);
                context.Users.Add(account);
                context.SaveChanges();
            }
            return 1;
        }

        public void UpdateAccount(RegisterAccount model)
        {
            
        }

        public RegisterAccount GetAccountById(int id)
        {
            return new RegisterAccount();
        }
    }
}