﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using SQLDatabase.Context;
using WebDZ_fixed.Interfaces;
using WebDZ_fixed.Models;
using AutoMapper;
using SQLDatabase.Models;
//using Client = WebDZ_fixed.Models.ClientViewModel;

namespace WebDZ_fixed.Services
{
    public class AdminService : IAdmin
    {
        TestApplicationContext context = new TestApplicationContext();

        public void CreateScore(ScoreViewModel model)
        {
            Mapper.CreateMap<ScoreViewModel, Score>();
            var score = Mapper.Map<ScoreViewModel, Score>(model);
            context.Scores.Add(score);
            context.SaveChanges();
        }

        public void CreateClient(ClientViewModel model)
        {
            Mapper.CreateMap<ClientViewModel, Client>()
                .ForMember(x => x.ScoreId, opt => opt.MapFrom(x => x.ScoreId));
            var client = Mapper.Map<ClientViewModel, Client>(model);
            context.Clients.Add(client);
            context.SaveChanges();
        }

        public void UpdateClient(ClientViewModel model)
        {
            var client = context.Clients.FirstOrDefault(x => x.Id == model.Id);
            if (client != null)
            {
                context.Entry(client).State = EntityState.Modified;
                Mapper.CreateMap<ClientViewModel, Client>()
            .ForMember(x => x.ScoreId, opt => opt.MapFrom(x => x.ScoreId));
                context.Clients.AddOrUpdate(client);
                context.SaveChanges();
            }
            
        }
        
    }
}