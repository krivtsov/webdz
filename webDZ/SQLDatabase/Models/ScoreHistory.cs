﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDatabase.Models
{
    public class ScoreHistory
    {
        [Required]
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ScoreId { get; set; }
        public virtual Client Client { get; set; }
        public virtual Score Score { get; set; }
        public int Summa { get; set; }
        public string DescriptionScore { get; set; }
    }
}
