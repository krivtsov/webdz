﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDatabase.Models
{
    [Table("Score")]
    public class Score
    {
        public int Id { get; set; }
        [Required]
        public string Currency { get; set; }

    }
}
