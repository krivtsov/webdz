namespace SQLDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableclientcolumnscoreId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Client", name: "Score_Id", newName: "ScoreId");
            RenameIndex(table: "dbo.Client", name: "IX_Score_Id", newName: "IX_ScoreId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Client", name: "IX_ScoreId", newName: "IX_Score_Id");
            RenameColumn(table: "dbo.Client", name: "ScoreId", newName: "Score_Id");
        }
    }
}
