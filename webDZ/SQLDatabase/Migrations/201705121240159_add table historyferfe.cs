namespace SQLDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtablehistoryferfe : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScoreHistories", "Score_Id", "dbo.Score");
            DropIndex("dbo.ScoreHistories", new[] { "Score_Id" });
            RenameColumn(table: "dbo.ScoreHistories", name: "Score_Id", newName: "ScoreId");
            AlterColumn("dbo.ScoreHistories", "ScoreId", c => c.Int(nullable: false));
            CreateIndex("dbo.ScoreHistories", "ScoreId");
            AddForeignKey("dbo.ScoreHistories", "ScoreId", "dbo.Score", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScoreHistories", "ScoreId", "dbo.Score");
            DropIndex("dbo.ScoreHistories", new[] { "ScoreId" });
            AlterColumn("dbo.ScoreHistories", "ScoreId", c => c.Int());
            RenameColumn(table: "dbo.ScoreHistories", name: "ScoreId", newName: "Score_Id");
            CreateIndex("dbo.ScoreHistories", "Score_Id");
            AddForeignKey("dbo.ScoreHistories", "Score_Id", "dbo.Score", "Id");
        }
    }
}
