namespace SQLDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addclientandscorestable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Score_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Score", t => t.Score_Id)
                .Index(t => t.Score_Id);
            
            CreateTable(
                "dbo.Score",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Currency = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Client", "Score_Id", "dbo.Score");
            DropIndex("dbo.Client", new[] { "Score_Id" });
            DropTable("dbo.Score");
            DropTable("dbo.Client");
        }
    }
}
