namespace SQLDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtablehistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScoreHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        Summa = c.Int(nullable: false),
                        DescriptionScore = c.String(),
                        Score_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Score", t => t.Score_Id)
                .Index(t => t.ClientId)
                .Index(t => t.Score_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScoreHistories", "Score_Id", "dbo.Score");
            DropForeignKey("dbo.ScoreHistories", "ClientId", "dbo.Client");
            DropIndex("dbo.ScoreHistories", new[] { "Score_Id" });
            DropIndex("dbo.ScoreHistories", new[] { "ClientId" });
            DropTable("dbo.ScoreHistories");
        }
    }
}
