namespace SQLDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumnusertable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "Email", c => c.String(nullable: false));
            AddColumn("dbo.User", "PhoneNumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "PhoneNumber");
            DropColumn("dbo.User", "Email");
        }
    }
}
