using SQLDatabase.Models;

namespace SQLDatabase.Context
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class TestApplicationContext : DbContext
    {
        public TestApplicationContext()
            : base("name=TestApplicationContext")
        {
        }

        public DbSet<User> Users { get; set; }  
        public DbSet<Score> Scores { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ScoreHistory> ScoreHistorys { get; set; }
    }

}