﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SQLDatabase.Context;
using SQLDatabase.Models;

namespace SQLDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            //using (TestApplicationContext context = new TestApplicationContext())
            //{
            //    var query = from client in context.Clients
            //                join score in context.Scores on client.Score.Id equals score.Id
            //                select new { Client = client, Score = score };


            //    var query1 = context.Clients.Join(context.Scores, client => client.Score.Id, score => score.Id,
            //        (client, score) => new { Client = client, Score = score });


            //    // inner join linq
            //    var query2 = context.Clients.Join(context.Scores, client => client.Id, score => score.Id,
            //        (client, score) => new { Client = client, Score = score });


            //    // inner join custom
            //    var innerJoin = context.Clients.InnerJoin(context.Scores, client => client.Id, score => score.Id,
            //        (client, score) => new { Client = client, Score = score });

            //    // full join custom
            //    var fullJoin = context.Clients.FullJoin(context.Scores, client => client.Id, score => score.Id,
            //        (client, score) => new {Client = client, Score = score}).ToList();

            //    // left join custom
            //    var leftJoin = context.Clients.LeftJoin(context.Scores, client => client.Id, score => score.Id,
            //        (client, score) => new { Client = client, Score = score }).ToList();

            //    // right join custom
            //    var RightJoin = context.Clients.RightJoin(context.Scores, client => client.Id, score => score.Id,
            //        (client, score) => new { Client = client, Score = score }).ToList();


            //    context.SaveChanges();
            //    Console.WriteLine();
            //}
        }
    }

    public static class LinqExtension
    {

        // inner join
        public static IEnumerable<TResult> InnerJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer,
            IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector) where TKey : IComparable<TKey>
        {
            foreach (var outerItem in outer)
                foreach (var innerItem in inner)
                    if (innerKeySelector != null && outerKeySelector != null)
                        if (outerKeySelector(outerItem).CompareTo(innerKeySelector(innerItem)) == 0)
                            yield return resultSelector(outerItem, innerItem);
        }

        // full join
        public static IEnumerable<TResult> FullJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer,
            IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector,
            Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, TInner, TResult>> resultSelector)
        {
            foreach (var outerItem in outer)
                foreach (var innerItem in inner)
                    yield return resultSelector.Compile()(outerItem, innerItem);
        }

        // left join
        public static IEnumerable<TResult> LeftJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer,
            IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector) where TKey : IComparable<TKey> where TInner : new()
        {
            var innerLookup = inner.ToLookup(innerKeySelector);
            foreach (var outerItem in outer)
                foreach (var innerItem in innerLookup[outerKeySelector(outerItem)].DefaultIfEmpty())
                    yield return resultSelector(outerItem, innerItem);
        }


        // right join
        public static IEnumerable<TResult> RightJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer,
            IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector,
            Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector) where TKey : IComparable<TKey> where TOuter : new()
        {
            var outerLookup = outer.ToLookup(outerKeySelector);
            foreach (var innerItem in inner)
                foreach (var outerItem in outerLookup[innerKeySelector(innerItem)].DefaultIfEmpty())
                    yield return resultSelector(outerItem, innerItem);
        }
    }
}
